# O dockerfile serve para que o docker faça build com instruções específicas
# FROM docker utiliza como base a imagem de um local, no caso é do docker hub
# A imagem do NODE
FROM node:14 

# Docker quero que você trabalhe dentro deste diretório
WORKDIR /usr/src/app

# Copiar a pasta local para dentro do contêiner
# da pasta atual, para o workdir
COPY . .

# Docker execute esse comando por isso o RUN
RUN npm install

# Docker exponha a porta 3000 para nossa aplicação ficar disponível nesta porta
EXPOSE 3000

# CMD executa um conjunto de comandos, neste caso é para subir nossa aplicação
CMD [ "npm", "start" ]